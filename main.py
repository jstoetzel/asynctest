#! /usr/bin/env python

'''
TODO
doctest: python -m doctest this_file.py -v
module docstring
'''

import asyncio
import foo


async def chain(name, fw):
    p = foo.Foo(name, fw)
    p1 = p.upload()
    p2 = await p.update()


async def main():
    names = 'bar baz'.split()
    fws = 'A B'.split()
    for fw in fws:
        await asyncio.gather(*(chain(name, fw) for name in names))


if __name__ == '__main__':
    asyncio.run(main())

