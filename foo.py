#! /usr/bin/env python

'''
TODO
doctest: python -m doctest this_file.py -v
module docstring
'''

import asyncio
import random
import time


class Foo:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def upload(self):
        print(f'uploading {self.a} to {self.b}')
        time.sleep(1)
        print(f'done uploading {self.a} to {self.b}')


    async def update(self):
        print(f'updating {self.a} to {self.b}')
        sleep = random.randint(3, 10)
        print(f'sleeping for {sleep} s')
        await asyncio.sleep(sleep)
        print(f'done updating {self.a} to {self.b}')


if __name__ == '__main__':
    pass

